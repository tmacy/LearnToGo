package lib

import (
	"fmt"
	"testing"
)

func TestCaesarEncode(t *testing.T) {
	test := []byte{'a', 'b', 'z', 'x', 'A', 'Y'}
	testRet := []byte{'d', 'e', 'c', 'a', 'D', 'B'}

	ret := CaesarEncode(test)

	for i := 0; i < len(ret); i++ {
		if ret[i] != testRet[i] {
			t.Log("Caesar  encode failed")
			t.Fail()
		}
	}
}
func TestCaesarDecode(t *testing.T) {
	testRet := []byte{'a', 'b', 'z', 'x', 'A', 'Y'}
	test := []byte{'d', 'e', 'c', 'a', 'D', 'B'}

	ret := CaesarDecode(test)

	for i := 0; i < len(ret); i++ {
		if ret[i] != testRet[i] {
			t.Log("Caesar  decode failed")
			t.Fail()
		}
	}
}

func TestXorEncode(t *testing.T) {
	test := []byte{'d', 'e', 'c', 'a', 'D', 'B'}

	ret := XorEncode(test, test)
	fmt.Printf(" ret :%v\n", ret)
	for i,a:= range ret {
		if a != 0 {
			t.Logf("XorEncode Failed :%v\n",i)
			t.Fail()
		}
	}
	ret = XorEncode(ret,test)
	fmt.Printf(" ret :%v\n", ret)

	for i,a:= range ret {
		if a != test[i] {
			t.Logf("XorEncode Failed :%v\n",i)
			t.Fail()
		}
	}
}
