package main

import (
	//	"bufio"
	"fmt"
	"io"
	"lib"
	"os"
)
const (
	KEY = "afaDAFfg"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Printf("%s needs a argument\n", os.Args[0])
		return
	}

	file, err := os.OpenFile(os.Args[1], os.O_RDWR, 0660)
	if err != nil {
		fmt.Printf("file open err :%v\n", err)
		return
	}

	defer file.Close()
	filestat, _ := file.Stat()

	buf := make([]byte, filestat.Size())

	//	reader := bufio.NewReader(file)
	//	writer := bufio.NewWriter(file)

	//	for {
	//		buf,err := reader.ReadBytes('\n') // 读取到\n结束，但是为什么seek的指针移到文件最后

	readn, err := file.Read(buf)
	if err != nil {
		if err == io.EOF {
			//	break
		}
		fmt.Printf("file read string err :%v\n", err)
		return
	}

	fmt.Printf("readn :%v\n", readn)
	offset, err := file.Seek(int64(-readn), os.SEEK_CUR)

	if err != nil {
		fmt.Printf("file seek err :%v\n", err)
		return
	}
	fmt.Printf("read offset :%v\n", offset)

//	writen, err := file.WriteAt(lib.CaesarEncode(buf), offset)
//	writen, err := file.WriteAt(lib.CaesarDecode(buf), offset)
	writen, err := file.WriteAt(lib.XorEncode(buf,[]byte(KEY)), offset)
	//		n,err := writer.Write(lib.CaesarEncode(buf))
	if err != nil {
		fmt.Printf("wriete to file err :%v\n", err)
	}
	//		writer.Flush()
	fmt.Printf("write to file %v bytes \n", writen)

}
