package even

func Even(i int) bool {
	return i%2 == 0
}

// 文件名称首字母大写，外部可见 到底你好
func ODD(i int) bool {
	return i%2 == 1
}
