package even

import "testing"

func TestEven(t *testing.T) {
	if !Even(2) {
		t.Log("2 should be even")
		t.Fail()
	}
}

func TestOdd(t *testing.T) {
	if !ODD(1) {
		t.Log("1 should be odd")
		t.Fail()
	}
}
