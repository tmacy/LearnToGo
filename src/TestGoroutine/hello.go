package main

import (
	"fmt"
	"runtime"
	//"time"
)

func main() {
	names := []string{"Eric", "Harry", "Robert", "jim", "Tracy", "James"}
	for _, name := range names {
		go func(who string) {
			//		time.Sleep(1 * time.Microsecond)
			fmt.Printf("Hello from :%s\n", who)
		}(name)
	}
	runtime.Gosched()
}
