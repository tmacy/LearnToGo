package main

import (
	"bytes"
	"fmt"
	"io"
	"os/exec"
)

func main() {
	cmd0 := exec.Command("echo", "-n", "My first command from golang")
	//	cmd0 := exec.Command("ps", "aux")

	stdout0, err := cmd0.StdoutPipe()
	if err != nil {
		fmt.Printf("Error :Can not obtain the stdout pipe for command No.0 %s\n", err)
		return
	}
	if err := cmd0.Start(); err != nil {
		fmt.Printf("Error:The command No.0 can not be startup :%s\n", err)
		return
	}

	var outputBuf0 bytes.Buffer

	for {

		tempOutput0 := make([]byte, 3)
		n, err := stdout0.Read(tempOutput0)
		if err != nil {
			if err == io.EOF {
				break
			} else {
				fmt.Printf("Error :Can't read data from the pipe :%s\n", err)
				return
			}
		}
		if n > 0 {
			outputBuf0.Write(tempOutput0[:n])
		}
	}
	fmt.Printf("%s\n", outputBuf0.String())

}
