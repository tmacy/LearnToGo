package main

import (
	"fmt"
	"os/exec"
)

func main() {
	cmd0 := exec.Command("echo", "My first command form golang.")
	/*
		stdout0, err := cmd0.StdoutPipe()
		if err != nil {
			fmt.Printf("Error :Can not obtain the stdout pipe for command No.0 %s\n", err)
			return
		}
	*/
	if err := cmd0.Start(); err != nil {
		fmt.Printf("Error:The command No.0 can not be startup :%s\n", err)
		return
	}
	/*
		output0 := make([]byte, 0)
		n, err := stdout0.Read(output0)
		if err != nil {
			fmt.Printf("Error :Can't read data from the pipe :%s\n", err)
			return
		}
		fmt.Printf("%d\n", n)
	*/

}
