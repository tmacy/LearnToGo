package main

import "fmt"

func main() {

	var a int8
	var b int8

	a = 2
	b = 3
	fmt.Printf("bit mode :\na :%b b :%b \n", a, b)
	fmt.Printf("a &^ b : forces specified bit of  a :%b to 0 by b's bit : %b\n %b\n", a, b, a&^b)
	a = 0x0c //1100
	b = 0x06 //0110
	fmt.Printf("a &^ b : forces specified bit of  a :%b to 0 by b's bit : %b\n %b\n", a, b, a&^b)
	//1000

	// ^ is bitwise complement

	fmt.Printf(" ^a is %b and ^a + a = %#v\n", ^a, ^a+a)

}
