package main

import "fmt"

func main() {
	l := new(List)

	for i := 0; i < 10; i++ {
		l.Push(Value(i))
	}

	for n := l.Front(); n != nil; n = n.Next() {
		fmt.Printf("%v ", n.Value)
	}

	fmt.Println()

	for v, err := l.Pop(); err == nil; v, err = l.Pop() {
		fmt.Printf("%v ", v)
	}
	fmt.Println()
}
