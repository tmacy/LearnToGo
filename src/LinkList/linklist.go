package main

import (
	"errors"
)

//实现了FIFO的linklist

type Value int

type Node struct {
	Value
	pre, next *Node
}

type List struct {
	head, tail *Node
}

var errEmpty = errors.New("List is empty")

func (l *List) Front() *Node {
	return l.head
}
func (n *Node) Next() *Node {
	return n.next
}

func (l *List) Push(v Value) *List {
	var n *Node = new(Node)
	n.Value = v

	if l.head == nil {
		l.head = n
	} else {
		l.tail.next = n
		n.pre = l.tail
	}
	l.tail = n
	return l
}

func (l *List) Pop() (v Value, err error) {
	if l.tail == nil {
		err = errEmpty
	} else {
		v = l.tail.Value
		l.tail = l.tail.pre
		if l.tail == nil {
			l.head = nil
		}
	}
	return v, err
}
