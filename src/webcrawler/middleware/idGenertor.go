package middleware

import (
	"sync"
)

const (
	UINT32MAX uint32 = 0xffffffff
)

type IdGenertor interface {
	GetUint32() uint32
}

type idBox struct {
	idmutex sync.Mutex
	num     uint32
}

func NewIdGenertor() *idBox {
	return &idBox{num: 1}
}

func (ib *idBox) GetUint32() (id uint32) {
	ib.idmutex.Lock()
	defer ib.idmutex.Unlock()
	ib.num++
	id = ib.num % UINT32MAX
	return
}
