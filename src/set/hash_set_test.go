package set

import (
	"runtime/debug"
	"testing"
)

func TestHashSetCreate(t *testing.T) {
	defer func() {
		if err := recover(); err != nil {
			debug.PrintStack()
			t.Errorf("fatal error :%s\n", err)
		}
	}()

	t.Log("Starting TestHashSetCreation...")
	hs := NewHashSet()
	t.Log("Create a HashSet value:%v\n", hs)
	if hs == nil {
		t.Errorf("The result of func NewHashSet is nil\n")
	}
	isSet := IsSet(hs)

	if !isSet {
		t.Errorf("The value of HashSet is not a set \n")
	} else {
		t.Logf("The HashSet value is a set\n")
	}
}

func TestHashSetAddLenAndContaints(t *testing.T) {
	testSetAddLenAndContaints(t, func() Set { return NewHashSet() }, "HashSet")
}

func testSetAddLenAndContaints(t *testing.T, newSet func() Set, typeName string) {
	t.Logf("Starting Test%sAdd ...\n", typeName)

	set := newSet()

	expectedElemMap := make(map[interface{}]bool)

	var result bool
	for i := 0; i < 10; i++ {
		t.Logf("Add %v to the %s value :%v.\n", i, typeName, set)
		result = set.Add(i)

		if expectedElemMap[i] && result {
			t.Errorf("ERROR: the element adding (%v -> %v) is successful but should be failing \n",
				i, set)
			t.FailNow()
		}
		if !expectedElemMap[i] && !result {
			t.Errorf("ERROR: the element adding (%v -> %v)is failing\n")
			t.FailNow()
		}
		expectedElemMap[i] = true
	}

	t.Logf("The %s value :%v Add pass.",typeName,set)

	expectedLen := len(expectedElemMap)

	if set.Len() != expectedLen{
		t.Errorf("ERROR : The Length of %s value %d is not %d \n",
		typeName,set.Len(),expectedLen)
		t.FailNow()
	}
	t.Logf("The len of %s value :%v is %d.",typeName,set,expectedLen)

	for k:= range expectedElemMap{
		if !set.Contains(k){
			t.Errorf("ERROR :The %s value %v do not contains:%v\n",set,typeName,k)
			t.FailNow()
		}
	}

	t.Logf("The Contains of %s value :%v  pass",typeName,set)
}

/*
func testHashSetLenAndContaints(t *testing.T){
	testSetLenAndContaints(t,func() Set{return NewHashSet()},"HashSet")
}

func testSetLenAndContaints(t *testing.T,newSet func()Set,typeName,string){
	t.Logf("Starting Test %s LenAndContaints...",typeName)
}
*/
