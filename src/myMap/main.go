package myMap

import "reflect"

type GenericMap interface {
	//获取给定键值对应的元素值，没有则返回nil
	Get(key interface{}) interface{}
	//添加键值对，并返回给定键值的旧元素，没有返回nil
	Put(key interface{}, elem interface{}) (interface{}, bool)
	//删除给定的键值对，并返回旧元素，没有返回nil
	Remove(key interface{}) interface{}
	//清楚所有键值对
	Clear()
	//获取键值对数量
	Len() int
	//判断是否含有给定的键值
	Contains(key interface{}) bool
	//获取已经排序的键值所组成的切片
	Keys() []interface{}
	//获取已排序的元素值所组成的切片
	Elems() []interface{}
	//获取已排序的键值对所组成的字典值
	ToMap() map[interface{}]interface{}
	//获取键的类型
	KeyType() reflect.Type
	//获取元素的类型
	ElemType() reflect.Type
}

type OrderedMap interface {
	//获取第一个键值，没有返回nil
	FirstKey() interface{}
	//获取最后一个键值，没有返回nil
	LastKey() interface{}
	//获取小于键值toKey的所有的键值所对应的OrderedMap
	HeadMap(toKey interface{}) OrderedMap
	//获取小于键值toKey并大于等于fromKey的所有对应键值对组成的OrderedMap
	SubMap(fromKey interface{}, toKey interface{}) OrderedMap
	//获取大于键值fromKey的所有的键值所对应的OrderedMap
	TailMap(fromKey interface{}) OrderedMap
}

type ConcurrentMap interface {
	GenericMap
}
