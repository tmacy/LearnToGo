package myLoadGen

import(
	"time"
)


type myGenerator struct {
	caller lib.Caller
	timeoutNs time.Duration
	lps		uint32
	durationNs time.Duration
	concurrency uint32
	tickets		lib.GoTickets
	stopSign	chan byte
	cancelSign	byte
	endSign		chan uint64
	callCount	uint64
	status		lib.GenStatus
	resultCh	chan *lib.CallResult
}
