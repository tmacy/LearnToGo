package lib

import (
	"time"
)

type RawReq struct {
	Id  int64
	Req []byte
}

type RawResp struct {
	Id     int64
	Resp   []byte
	Err    error
	Elapse time.Duration
}

type CallResult struct {
	Id     int64
	Req    RawReq
	Resp   RawResp
	Code   ResultCode
	Msg    string
	Elapse time.Duration
}

type GenStatus int

const (
	STATUS_ORIGINAL GenStatus = 0
	STATUS_STARTED  GenStatus = 1
	STATUS_STOPPED  GenStatus = 2
)

type ResultCode int

const (
	RESULT_CODE_SUCCESS                         = 0
	RESULT_CODE_WARNING_CALL_TIMEOUT ResultCode = 1001
	RESULT_CODE_ERROR_CALL           ResultCode = 2001
	RESULT_CODE_ERROR_RESPONSE       ResultCode = 2002
	RESULT_CODE_ERROR_CALLEE         ResultCode = 2003
	RESULT_CODE_FATAL_CALL           ResultCode = 3001
)

func GetResultCodePlain(code ResultCode) string {
	var codePlain string

	switch code {
	case RESULT_CODE_SUCCESS:
		codePlain = "Sucess"
	case RESULT_CODE_WARNING_CALL_TIMEOUT:
		codePlain = "Call Timeout Wraning"
	case RESULT_CODE_ERROR_CALL:
		codePlain = "Call error"
	case RESULT_CODE_ERROR_RESPONSE:
		codePlain = "Response Error"
	case RESULT_CODE_ERROR_CALLEE:
		codePlain = "Call Fatal Error"
	case RESULT_CODE_FATAL_CALL:
		codePlain = "Call Fatal Error"
	default:
		codePlain = "Unknown result code"
	}
	return codePlain
}

type Generator interface {
	Start()
	Stop() (uint64, bool)
	Status() GenStatus
}
