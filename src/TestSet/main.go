package main

import (
	"fmt"
	"set"
)

func main() {
	hs := set.NewHashSet()
	if hs == nil {
		fmt.Println("create HashSet error")
	}

	fmt.Println(hs.Len())
	hs.Add("你好")
	hs.Add(3.1415926)
	hs.Add("hello")
	hs.Add("Go 语言")
	fmt.Println(hs.Len())
	fmt.Println(hs.Elements())

	hs.Remove("你好")
	fmt.Println(hs.Elements())

	if hs.Contains("hello") {
		fmt.Println("hs has an element about hello")
	}
	fmt.Println(hs.String())
	hs.Clear()
	fmt.Println(hs.String())

	//判断&set.HashSet是否是一个set.Set接口
	if _, ok := interface{}(&set.HashSet{}).(set.Set); ok {
		fmt.Println("set.HashSet is a Set")
	}

}
