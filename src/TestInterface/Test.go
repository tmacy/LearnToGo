package main

import "fmt"
import "sort"

type Sortable interface {
	sort.Interface
	Sort()
}

type SortableString []string

func main() {

	if _, ok := interface{}(SortableString{}).(sort.Interface); ok {
		fmt.Println("SortableString is sort.Interface")
	}

	if _, ok2 := interface{}(SortableString{}).(Sortable); ok2 {
		fmt.Println("SortableString is Sortable")
	}

	ss := SortableString{"3", "2", "1"}

	ss.Sort()

	fmt.Printf("SortableString ss :%v\n", ss)

}

func (self SortableString) Len() int {
	return len(self)
}

func (self SortableString) Less(i, j int) bool {
	return self[i] < self[j]
}

func (self SortableString) Swap(i, j int) {
	self[i], self[j] = self[j], self[i]
}
func (self SortableString) Sort() {
	sort.Sort(self)
}
