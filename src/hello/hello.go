package main

import (
	"fmt"
)

var a string

func main() {
    /*
	var i int

	for {
		fmt.Scanf("%d", &i)
		switch i {
		case 0:
			fmt.Println("chose the 0")
		case 1:
			fmt.Println("chose the 1")
		case 2:
		case 3:
			fmt.Println("chose the 3")
			fallthrough
		case 4:
			fmt.Println("from 3 fallthrough to 4")
		case 5, 6, 7, 8, 9:
			fmt.Println("chose 5 - 9")
		default:
			fmt.Println("this is the default")
		}
	}
*/

    fmt.Printf("a test : \n")
    a = "G"
    print(a)
    f1()
    fmt.Println()
}

func f1(){
    a := "O"
    print(a)
    f2()
}

func f2(){
    print(a)
}
