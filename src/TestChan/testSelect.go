package main

import (
	"fmt"
)

func main() {

	chanCap := 5
	ch1 := make(chan int, 6)

	for i := 0; i < chanCap; i++ {
		select {
		case ch1 <- 1:
		case ch1 <- 2:
		case ch1 <- 3:
		default:
			fmt.Println("default")
		}

	}

	for i := 0; i < chanCap; i++ {
		fmt.Printf("%v\n", <-ch1)
	}
}
