package main

import (
	"fmt"
	"time"
)

func main() {
	ch := make(chan int, 5)
	sign := make(chan byte, 2)

	go func() {
		for i := 0; i < 5; i++ {
			ch <- i
			time.Sleep(1 * time.Second)
		}
		close(ch)
		fmt.Printf("the channel is closed\n")
		sign <- 0
	}()

	go func() {
		for {
			i, ok := <-ch
			if ok {
				fmt.Printf("%d (%v)\n", i, ok)
				time.Sleep(2 * time.Second)
			} else {
				break
			}
		}
		fmt.Printf("Done.\n")
		sign <- 1
	}()

	<-sign
	<-sign
}
