package main

import (
	"bufio"
	"fmt"
	"io"
	"os/exec"
)

func main() {
	cmd := exec.Command("ps", "aux")

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		fmt.Printf("Error :Can't obtain the stdout pipe for command %s\n", err)
		return
	}

	if err := cmd.Start(); err != nil {
		fmt.Printf("Error :The command can not be startup :%s\n", err)
		return
	}

	outBuf := bufio.NewReader(stdout)

	for {
		output, _, err := outBuf.ReadLine()
		if err != nil {
			if err == io.EOF {
				break
			}
			fmt.Printf("Error:Can't read data from the pipe :%s\n", err)
			return
		}
		fmt.Printf("%s\n", string(output))
	}
}
