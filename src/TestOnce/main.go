package main

import (
	"fmt"
	"sync"
	"time"
)

//运行一次
func main() {
	var num int

	sign := make(chan bool)
	var once sync.Once

	f := func(i int) func() {
		return func() {
			num = (num + i*2)
			sign <- true
		}
	}

	for i := 0; i < 3; i++ {
		fi := f(i + 1)
		go once.Do(fi)
	}

	for j := 0; j < 3; j++ {
		select {
		case <-sign:
			fmt.Printf("receive a signal.\n")
		case <-time.After(100 * time.Millisecond):
			fmt.Println("Timeout!")
		}
	}
	fmt.Printf("Num :%d\n", num)
}
