package main

import (
	"fmt"
	"reflect"
)

type GenericMap interface {
	Get(key interface{}) interface{}

	Put(key interface{}, elem interface{}) (interface{}, bool)

	Remove(key interface{}) interface{}

	Clear()

	Len() int

	Contains(key interface{}) bool

	Keys() []interface{}

	Elems() []interface{}

	ToMap() map[interface{}]interface{}

	KeyType() reflect.Type

	ElemType() reflect.Type
}

type OrderedMap interface {
	GenericMap

	FirstKey() interface{}

	LastKey() interface{}

	HeadMap(toKey interface{}) OrderedMap

	SubMap(toKey interface{}, fromKey interface{}) OrderedMap

	TailMap(fromKey interface{}) OrderedMap
}
